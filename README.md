# Read me

## Part 2A

## TODO: Rewrite

app.py, does a lot of different things, managing users logins, sending and receving data, and so on.
Even though there are a lot of comments in app.py, there are few docstrings, which if included, makes the life of the next programmer, easier, so I will add some of those. And, we have usernames and their corresponding passwords in plaintext, which needs to be fixed.

When gettin users, we use their usernames.

### Problems

- Security issues:
  - Unsanitized sql querries.
  - Password stored as a string

- Coding practices:
  - One file with a lot of responsibility
  - No docstrings
  - No typping

### Fixes

app.py, which was supposed to be smaller, is somehow larger
Has a large file specifically for database functions
Has a file for hashing and comparing hashes, and getting a random string for session_key.
SQL injection is prevented, by preventing the user from sending in specific chars.
Passwords are hashed, and not printed anywhere.
Implemented Flask session and csrf protection.

## Part 2B

### Features

In no order:

Contains both time and date of the message, aswell as an id.
A user can only see messages that they are in, i.e. if they are sending the message, or if they are reciving it,
Users can block other users, and blocked users cannot send messages to the ones who blocked them.
Users can also find and message offline and online users, block and unblock them.
Users can add other users as friends, which get a specifc tab, where one can easier keep track of their messages.
These are specific to DM's, so no group message is shown here. Definitivly a feature, not a bug.

You can send a DM to a specific user, by just pressing "MESSAGE"; This takes the user to a form where they can write a message to send.
(This is very unsafe, as anyone can send messages as anyone, to anyone, regardless if they are blocked and not, this could be fixed, if I instead of sent the user to a form, used some html/css/js code to get the input on the same webage, and then could use Flask.sessions, but I was too hooked on the idea of creating a DM button, to realise this, until now.)

You cannot unfriend people directly, but you can block friends, which will unfriend them, and then unblock them, this is a feature.

The reason it takes 1.5 seconds to unblock, block and befriend users, is that without the wait, the html/css/js weirded out, it would not reload the friend buttons, but once the wait was added, it worked, this is therefore a feature.

Searching now only shows the currently logged in user, and if any thing thats put in the search field, is searched on senders,
recivers and in the message, but using SQLLite's searching rules, i.e. * for wildstar and such.

You can check the status of other users; if they are online or not. This is done by checking if a session has been ended.

Once a user is logged in, the "From" label is set to the user thats logged in.
This is not changed if you open up two pages in a web browser and log in on another account, which will give weird stuff.
For example, if logged in as Alice, then you open a new web browser, you'll be logged in as Alice, but if you log out, log in as another user like Bob,
the original web browser, where it will say youre logged in as Alice, you'll be able to see Bob's messages if you click search, but send messages as Alice.
This is a feature, to showcase that you should always log off before giving the pc to another person.

Passwords are stored as hashes, using bcrypt, and the plaintext password is not displayed anywhere.
A secret_key for session is randomly generated everytime the server is restarted, this could be changed.

The Database is in debug mode by default, which means it will drop all tables on start, I have a txt file which has some random data which is put into the database, but if this is not desired, simply change this line `30`, in `app.py`, from this: `init_db(conn)` to this `init_db(conn, False)`. This will disable the dropping and inputting of data at start up.

#### How to Demo

Log into any of the users in the test_users.txt, (if init_db is unchanged), and mess away.
If you want too, you can log onto multiple users at the same time, using incognito web browers.
The messages are not updated in realtime, however, so use the search button, refresh the page, or cycle between the friends.
If you block a user, they cannot send a message to you.
Being friends with a user creates a specific tab for them.
Clicking the "Message" button on the friends tab, reveals a ~~major security flaw~~ major feature.
User status are also not updated in realtime, so refresh the tab to see if anyone comes online.

Closing a browser should end a session, as in `index.html`, we have a `window.onbeforeunload`, which __should__ call the `logout` function in `app.py`.
SQL injections should be fixed, as you can only type a message with ascii.chars, digits and ' !_-.,:;*\n\t'. Best solution to a security issue, remove things the user can do.

### Questions

#### Threat model

##### Confidentiality

If an attacker got to the mainframe, and hacked into the database by typing `sudo pacman -Syu`, one could see every message, its recipients, sender and the context of the message, which is a privaty issue, too fix this, one could hash the message, and store the bytes of the hash, hash the username, and store it as bytes, and actually use primary keys, this was not done, as I have more experience in Python than databases, and at the time of coding, it is easier if one can actually read the database.

##### Integrity

##### Availability

You can DDOS this, there is no built in way to stop this. To deny DNS, a cap on the length of a message has been added, but a timer might also be a good idea.

#### Main attack vectors

XSS would work, as I have not grasped the concept, and therefore added nothing to make the website more secure.
CRSF might not work, as I have added Flasks, CRSF protection.
SQL injection would not work, as there are a limit on what chars are allowed.

As stated earlier, typing `http://localhost:5000/send_dm?reciver=R&sender=S`, switching R and S for whatever user one wants, anyone can send a message to anyone, as anyone, regardles of their blocked status. Easy fix for this, is to remove this form, I did not because I spent a lot of time on it.

#### Traceability

I cannot say for sure here, because I did not take into account logging, which I probably should.

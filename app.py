from http import HTTPStatus
from flask import Flask, abort, jsonify, request, send_from_directory, make_response, render_template
from werkzeug.datastructures import WWWAuthenticate
import flask
from database import get_session_key
from forms import LoginForm, MessageForm
from base64 import b64decode
import sys
import apsw
from apsw import Error
from pygments import highlight
from pygments.lexers import SqlLexer
from pygments.formatters import HtmlFormatter
from pygments.filters import NameHighlightFilter, KeywordCaseFilter
from pygments import token
from threading import local
from markupsafe import escape
from flask_wtf.csrf import CSRFProtect
tls = local()
inject = "'; insert into messages (sender,message) values ('foo', 'bar');select '"
cssData = HtmlFormatter(nowrap=True).get_style_defs('.highlight')

# Set up app

def main():
    from database import init_db
    try:
        global conn
        conn = apsw.Connection('./tiny.db')
        init_db(conn)
    except Error as e:
        print(e)
        sys.exit(1)
    
main()
csrf = CSRFProtect()
app = Flask(__name__)
session_key = get_session_key(conn)
app.secret_key = session_key
csrf.init_app(app)

# Add a login manager to the app
import flask_login
from flask_login import AnonymousUserMixin, login_required, login_user, logout_user
login_manager = flask_login.LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"


users = {'alice' : {'password' : 'password123', 'token' : 'tiktok'},
         'bob' : {'password' : 'bananas'}
         }

# Class to store user info
# UserMixin provides us with an `id` field and the necessary
# methods (`is_authenticated`, `is_active`, `is_anonymous` and `get_id()`)
class User(flask_login.UserMixin):
    pass


# This method is called whenever the login manager needs to get
# the User object for a given user id
@login_manager.user_loader
def user_loader(user_id):
    from database import is_existing_user, get_user
    
    if user_id is not None and is_existing_user(conn, user_id):
        return get_user(conn, user_id)


# This method is called to get a User object based on a request,
# for example, if using an api key or authentication token rather
# than getting the user name the standard way (from the session cookie)
@login_manager.request_loader
def request_loader(request):
    # Even though this HTTP header is primarily used for *authentication*
    # rather than *authorization*, it's still called "Authorization".
    auth = request.headers.get('Authorization')

    # If there is not Authorization header, do nothing, and the login
    # manager will deal with it (i.e., by redirecting to a login page)
    if not auth:
        return

    (auth_scheme, auth_params) = auth.split(maxsplit=1)
    auth_scheme = auth_scheme.casefold()
    if auth_scheme == 'basic':  # Basic auth has username:password in base64
        (uid,passwd) = b64decode(auth_params.encode(errors='ignore')).decode(errors='ignore').split(':', maxsplit=1)
        u = users.get(uid)
        if u: # and check_password(u.password, passwd):
            return user_loader(uid)
    elif auth_scheme == 'bearer': # Bearer auth contains an access token;
        # an 'access token' is a unique string that both identifies
        # and authenticates a user, so no username is provided (unless
        # you encode it in the token – see JWT (JSON Web Token), which
        # encodes credentials and (possibly) authorization info)
        for uid in users:
            if users[uid].get('token') == auth_params:
                return user_loader(uid)
    # For other authentication schemes, see
    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Authentication

    # If we failed to find a valid Authorized header or valid credentials, fail
    # with "401 Unauthorized" and a list of valid authentication schemes
    # (The presence of the Authorized header probably means we're talking to
    # a program and not a user in a browser, so we should send a proper
    # error message rather than redirect to the login page.)
    # (If an authenticated user doesn't have authorization to view a page,
    # Flask will send a "403 Forbidden" response, so think of
    # "Unauthorized" as "Unauthenticated" and "Forbidden" as "Unauthorized")
    abort(HTTPStatus.UNAUTHORIZED, www_authenticate = WWWAuthenticate('Basic realm=inf226, Bearer'))

@app.route('/favicon.ico')
def favicon_ico():
    return send_from_directory(app.root_path, 'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route('/favicon.png')
def favicon_png():
    return send_from_directory(app.root_path, 'favicon.png', mimetype='image/png')


@app.route('/')
@app.route('/index.html')
@login_required
def index_html():
    return send_from_directory(app.root_path,
                        'index.html', mimetype='text/html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    from database import is_existing_user, check_password, add_session, get_session_key
    
    form = LoginForm()
    if form.validate_on_submit():
        username = form.username.data
        user_input_str = form.password.data
        
        # User does not exist
        if not is_existing_user(conn, username):
            print(f"user: {username} does not exist")
            return render_template('./login.html', form=form)
        
        
        # Checks password
        if check_password(conn, username, user_input_str):
                    
            user = user_loader(username)
            
            
            flask_login.current_user.id = username
            
            # automatically sets logged in session cookie
            login_user(user)

            flask.flash('Logged in successfully.')
            flask.flash('Logged in successfully.')

            next = flask.request.args.get('next')
    
            # is_safe_url should check if the url is safe for redirects.
            # See http://flask.pocoo.org/snippets/62/ for an example.
            if False and not is_safe_url(next):
                return flask.abort(400)
            
            add_session(conn, username, app.secret_key)
            
            return flask.redirect(next or flask.url_for('index_html'))
        else:
            print("Password is not the same")
    return render_template('./login.html', form=form)

@app.route('/logout', methods=['POST', 'GET'])
def logout():
    from database import end_session
    end_session(conn, flask_login.current_user.id, app.secret_key)
    logout_user()
    return 'LOGGED OUT'

@app.get('/search')
def search():
    from database import search_messages, validate_query
    query = request.args.get('q') or request.form.get('q') or '*'
    result = ""
    try:
        if validate_query(query):
            rows = search_messages(conn, flask_login.current_user.id, query)
        else:
            return (f'{result}ERROR: {ValueError}', 500)
        
        for id, date, time, sender, reciver, message in rows:
            result += f"Message ID: {id}, Date: {date}, Time: {time}\nFrom: {sender}\nTo: {reciver}\nMessage:\n{message}\n\n"
        return result
    except Error as e:
        return (f'{result}ERROR: {e}', 500)

@app.route('/send', methods=['POST','GET'])
def send():
    from database import add_message, validate_query, is_valid_message, is_blocked, is_existing_user
    try:
        sender = request.args.get('sender') or request.form.get('sender')
        message = request.args.get('message') or request.args.get('message')
        reciver = request.args.get("reciver") or request.args.get("reciver")
        if not sender or not message:
            return f'ERROR: missing sender or message'
        
        if not is_valid_message(message):
            return "NOT VALID MESSAGE"
        
        for rec in reciver.split(", "):
            if is_existing_user(conn, rec):
                if is_blocked(conn, sender, rec):
                    print(f"Cannot send message to {rec}, is blocked")
        
        if validate_query(sender + reciver + message):
            add_message(conn, sender, reciver, message)
            return f"{message}"
        else:
            return "Not valid query Error."
    except Error as e:
        return f'ERROR: {e}'

@app.get('/announcements')
def announcements():
    try:
        stmt = f"SELECT author,text FROM announcements;"
        # SQL Injection?
        c = conn.execute(stmt)
        anns = []
        for row in c:
            anns.append({'sender':escape(row[0]), 'message':escape(row[1])})
        return {'data':anns}
    except Error as e:
        return {'error': f'{e}'}

@app.get('/coffee/')
def nocoffee():
    abort(418)

@app.route('/coffee/', methods=['POST','PUT'])
def gotcoffee():
    return "Thanks!"

@app.get('/highlight.css')
def highlightStyle():
    resp = make_response(cssData)
    resp.content_type = 'text/css'
    return resp

@app.route("/test", methods=['GET', 'POST'])
def temp():
    if request.method == "GET":
        return jsonify({"txt" : flask_login.current_user.id})  # serialize and use JSON headers
    
    if request.method == 'POST':
        return 'Sucesss', 200

@app.route("/get_friends", methods=['GET'])
def get_friends():
    from database import get_user_friends, is_user_online
    
    if request.method == "GET":
        friends = get_user_friends(conn, flask_login.current_user.id)
        f_dict = {}
        lst = []
        if friends is None:
            return jsonify({"friends":[]})
        for friend in friends:
            f_info = {}
            _, f_name, _, _, _, f_fact = friend
            f_info["name"] = f_name
            f_info["fact"] = f_fact
            f_info["online"] = is_user_online(conn, f_name)
            lst.append(f_info)
        f_dict["friends"] = lst
        return jsonify(f_dict)
    
@app.get("/is_online")
def is_online():
    from database import is_user_online
    user = request.args.get("user")
    d = {}
    d["status"] = str(is_user_online(conn, user))
    return jsonify(d)

@app.get("/add_friend")
def add_friend_to_user():
    from database import add_friend
    user = flask_login.current_user.id
    friend = request.args.get("friend")
    add_friend(conn, user, friend)
    return "Ok"
    
@app.get("/block_user")
def block_user_():
    from database import block_user
    user = flask_login.current_user.id
    block = request.args.get("blocked_user")
    block_user(conn, user, block)
    return "Ok"
   
@app.get("/unblock_user")
def unblock_user_():
    from database import unblock_user
    user = flask_login.current_user.id
    block = request.args.get("blocked_user")
    unblock_user(conn, user, block)
    return "Ok"


@app.get("/get_messages")
def get_messages():
    from database import get_message
    user = flask_login.current_user.id
    contact = request.args.get("user")
    messages = get_message(conn, user, contact)
    d = {"form" : user}
    lst = []
    for id, date, time, sender, recivers, message in messages:
        msg = {
            "date" : date,
            "time" : time,
            "text" : message,
            "from" : sender,
            "recivers" : ";".join(recivers.split("|"))
        }
        lst.append(msg)
    d["messages"] = lst
    return jsonify(d)

@app.route("/send_dm", methods=(["GET", "POST"]))
def send_direct_message():
    from database import is_valid_message, add_message
    recivers = request.args.get("reciver")
    sender = request.args.get("sender")    
    form = MessageForm()
    form.sender.text = f"From: {sender}"
    form.reciver.label = f"To: {recivers}"
    
    
    if form.validate_on_submit():
        recivers = form.reciver.data if form.reciver.data is not None else recivers
        msg = form.message.data
        if is_valid_message(msg):
                add_message(conn, sender, recivers, msg)
        return flask.redirect(flask.url_for('index_html'))
            
    return render_template('./message.html', form=form)


@app.get("/online_users")
def online_users():
    from database import get_all_online_users
    d = {}
    lst = []
    res = get_all_online_users(conn)
    if res is None:
        return jsonify({"users":[]})
    for _, user, *_ in res:
        if user == flask_login.current_user.id:
            continue
        msg = {
            "user" : user
        }
        lst.append(msg)
    d["users"] = lst
    return jsonify(d)

@app.get("/offline_users")
def offline_users():
    from database import get_all_offline_users
    d = {}
    lst = []
    res = get_all_offline_users(conn)
    if res is None:
        return jsonify({"users":[]})
    for _, user, *_ in res:
        msg = {
            "user" : user
        }
        lst.append(msg)
    d["users"] = lst
    return jsonify(d)

@app.get("/blocked_users")
def blocked_users():
    from database import get_blocked
    d = {}
    lst = []
    res = get_blocked(conn, flask_login.current_user.id)
    
    if res is None or res == [None] or res == []:
        return jsonify({"users":[]})
    for _, user, *_ in res:
        msg = {
            "user" : user
        }
        lst.append(msg)
    d["users"] = lst
    return jsonify(d)


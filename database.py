from __future__ import annotations
from apsw import Connection
import string
from hash_input import compare_hashes, hash_string, random_str
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from app import User
from datetime import datetime as dt

VALID_CHARS = string.ascii_letters + string.digits + " !_-.,:;*\n\t"
SEPERATOR = "|"
MESSAGE_LIMIT = 400
NON_VALID_USERNAMES = ["NULL", "VOID", "NONE", "null", "None", "Null"]

def init_db(db: Connection, debug: bool = True) -> None:
    """Initilases the database

    Args:
        db (Connection): apsw.Connection
    """
    c = db.cursor()
    if debug:
        c.execute("CREATE TABLE IF NOT EXISTS users (id INTEGER)")
        c.execute("DROP TABLE users")
        c.execute("CREATE TABLE IF NOT EXISTS sessions (id INTEGER)")
        c.execute("DROP TABLE sessions")
        c.execute("CREATE TABLE IF NOT EXISTS messages (id INTEGER)")
        c.execute("DROP TABLE messages")
    
    # Message database
    c.execute('''CREATE TABLE IF NOT EXISTS messages (
        id integer PRIMARY KEY,
        date TEXT NOT NULL,
        time TEXT NOT NULL,
        sender TEXT NOT NULL,
        recivers TEXT NOT NULL,
        message TEXT NOT NULL);''')
    # Adds test messages
    if debug:
        with open("test_message.txt", "r") as f:
            for l in f.readlines():
                date, time, sender, recivers, msg = l.split(";")
                msg = msg.splitlines()[0]
                c.execute("INSERT INTO messages (date, time, sender, recivers, message) VALUES ('{date}', '{time}', '{sender}', '{recivers}', '{msg}')")
    # Announcement database
    c.execute('''CREATE TABLE IF NOT EXISTS announcements (
        id integer PRIMARY KEY, 
        author TEXT NOT NULL,
        text TEXT NOT NULL);''')
    # User database
    c.execute("""CREATE TABLE IF NOT EXISTS users
              (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
              username TEXT NOT NULL,
              password BLOB NOT NULL,
              friends TEXT,
              blocked TEXT,
              funfact TEXT);""")
    # Inserts test users
    if debug:
        with open("test_users.txt", "r") as f:
            for l in f.readlines():
                name, pswd, friends, blocked, fact = l.split(";")
                fact = "".join(fact.splitlines())
                friends = friends if friends != "" else "NULL"
                blocked = blocked if blocked != "" else "NULL"
                fact = fact if fact != "" else "NULL"
                hashed_pswd = hash_string(pswd).decode("utf-8")
                c.execute(f"INSERT INTO users (username, password, friends, blocked, funfact) VALUES ('{name}', '{hashed_pswd}', '{friends}', '{blocked}', '{fact}')")
    # Session
    c.execute("""CREATE TABLE IF NOT EXISTS sessions
              (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
              session_key BLOB NOT NULL,
              user TEXT NOT NULL,
              session_start TEXT NOT NULL,
              session_end TEXT);
              """)
    

def check_password(db: Connection, username: str, password: str) -> bool:
    """Checks if the given username corresponds to an actual user,
    and if the password is correct

    Args:
        db (Connection): apsw.Connection
        username (str): Given username
        password (str): Given, password

    Returns:
        bool: True, if the passwords are the same.
    """
    passwd = ""
    # Avoids sql injection
    for u_name, ps in db.cursor().execute("SELECT username, password from users"):
        if u_name == username:
            passwd = ps
    return compare_hashes(password.encode("utf-8"), passwd.encode("utf-8"))


def add_user(db: Connection, username: str, password: bytes) -> None:
    """Adds a user to the given database

    Args:
        db (Connection): apsw.Connection
        username (str): Username
        password (bytes): Password
    """
    # Avoids sql injection
    db.cursor().execute(f"INSERT INTO users (username, password) VALUES ('{username}', '{password}')")
    
    
def search_messages(db: Connection, username: str, query: str) -> list[str]:
    """Finds the messages based on the given parameters

    Args:
        db (Connection): apsw.Connection
        query (str): Parameters

    Returns:
        list[str]: List of the rows that fullfill the parameters
    """
    # Sanatizes user input
    
    # Checks if a user
    
    lst = []
    for row in db.cursor().execute(f"SELECT * FROM messages WHERE sender GLOB '{query}'"): # query = *'; DROP TABLE users;--        lst.append(row)
        _, _, _, sender, recivers, _ = row
        if sender == username or username in recivers:
            lst.append(row)
    
    for row in db.cursor().execute(f"SELECT * FROM messages WHERE recivers GLOB '{query}'"): # query = *'; DROP TABLE users;--        lst.append(row)
        _, _, _, sender, recivers, _ = row
        if sender == username or username in recivers:
            lst.append(row)
    
    for row in db.cursor().execute(f"SELECT * FROM messages WHERE message GLOB '{query}'"): # query = *'; DROP TABLE users;--        lst.append(row)
        _, _, _, sender, recivers, _ = row
        if sender == username or username in recivers:
            lst.append(row)
            
    lst = set(lst)
    return list(lst)


def is_valid_message(message: str) -> bool:
    """Checks that the message is valid

    Args:
        message (str): Message

    Returns:
        bool: True if a valid message
    """
    return validate_query(message) and len(message) <= MESSAGE_LIMIT and len(message) != 0


def add_message(db: Connection, sender: str, recivers: str, message: str) -> None:
    """Adds the message to the database

    Args:
        db (Connection): apsw.Connection
        sender (str): User
        message (str): Message
    """
    now = dt.now()
    date = now.strftime("%d.%m.%y")
    time = now.strftime("%H:%M")
    
    users = recivers.split(", ")
    
    users = [u for u in users if is_existing_user(db, u) and not is_blocked(db, sender, u)]
    
    # Only stores the message, if it has any recivers, if there is any message, and if it the message is not only whitespace
    if len(users) == 0 or len(message) == 0 or message.count(" ") == len(message) or not is_existing_user(db, sender):
        return
    
    res = SEPERATOR.join(users)
    # Sanatizes user input before I execute
    db.cursor().execute(f"INSERT INTO messages (date, time, sender, recivers, message) VALUES ('{date}', '{time}', '{sender}', '{res}', '{message}')")
    
    
def validate_query(query: str) -> bool:
    """Checks if a query is valid or not, i.e. it only contains valid chars

    Args:
        query (str): Query to validate

    Returns:
        bool: True if a valid query
    """
    for c in query:
        if c not in VALID_CHARS:
            return False
    return True


def is_existing_user(db: Connection, username: str) -> bool:
    """Checks the database if the given username exists

    Args:
        db (Connection): apsw.Connection
        username (str): Username

    Returns:
        bool: Returns true if username exists
    """
    return any([r[0] == username for r in db.cursor().execute("SELECT username FROM users")])


def get_user(db: Connection, username: str) -> User:
    """Returns a User instance, if the given username exists, else returns None
    Session key is not init. here!

    Args:
        db (Connection): apsw.Connection
        username (str): Username

    Returns:
        User: User instance
    """
    from app import User
    
    for row in db.cursor().execute("SELECT username FROM users"):
        if row[0] == username:
            u = User()
            u.id = row[0]
            return u


def add_session(db: Connection, username: str, session_key: str) -> None:
    """Adds the session to the database

    Args:
        db (Connection): apsw.Connection
        username (str): username
        session_key (str): session key
    """
    
    time_start = dt.now().strftime("%H:%M %d.%m.%y")
    
    db.cursor().execute(f"INSERT INTO sessions (session_key, user, session_start, session_end) VALUES ('{session_key}', '{username}', '{time_start}', 'NULL')")


def get_user_session(db: Connection, username: str) -> str:
    """Gets the user session key, should always be a result from this function,
    unless something is wrong :)

    Args:
        db (Connection): apsw.Connection
        username (str): Username

    Returns:
        str: Session key
    """
    for s_key, u_name, s_end in db.cursor().execute("SELECT session_key, user, session_end FROM sessions"):
        if u_name == username and s_end == "NULL":
            return s_key

def end_session(db: Connection, username: str, session_key: str) -> None:
    """Ends the session of the user

    Args:
        db (Connection): aspw.Connection
        username (str): Username
        session_key (str): session key
    """
    # Finds the session
    for id, s_key, u_name, s_start, s_end in db.cursor().execute("SELECT * FROM sessions"):
        if s_key == session_key and u_name == username and s_end == "NULL":
            db.cursor().execute(f"""DELETE FROM sessions WHERE id = {id}""") # Removes the row
            s_end = dt.now().strftime("%H:%M %d.%m.%y") # session end time
            # Adds the row
            db.cursor().execute(f"INSERT INTO sessions (id, session_key, user, session_start, session_end) VALUES ('{id}', '{s_key}', '{username}', '{s_start}', '{s_end}')")

def is_session_ended(db: Connection, session_key: str) -> bool:
    """Checks if the given session is ended, can give false positives, i.e. if a user has no sessions
    Should not happen, but could

    Args:
        db (Connection): apsw.Connection
        session_key (str): Session key

    Returns:
        bool: True if it's ended
    """
    for s_key, s_end in db.cursor().execute("SELECT session_key, session_end FROM sessions"):
        if s_key == session_key and s_end == "NULL":
            return True
    return False


def is_valid_session_key(db: Connection, session_key: str) -> bool:
    """Checks if the given session key is valid, i.e. it does not already exist

    Args:
        db (Connection): _description_
        session_key (str): _description_

    Returns:
        bool: _description_
    """
    for s_key in db.cursor().execute("SELECT session_key FROM sessions"):
        if s_key[0] == session_key:
            return False
    return True
    

def get_session_key(db: Connection) -> str:
    """Returns a valid session key

    Args:
        db (Connection): apws.Connection

    Returns:
        str: session key
    """
    s_key = random_str()
    while not is_valid_session_key(db, s_key):
        s_key += random_str(1)
    return s_key


def get_user_by_session_key(db: Connection, session_key: str) -> User:
    """Gets a User based on session key, can be None

    Args:
        db (Connection): apws.Connection
        session_key (str): Session key

    Returns:
        User: User instance
    """
    for _, s_key, u_name, _, s_end in db.cursor().execute("SELECT * FROM sessions"):
        if s_end == "NULL" and s_key == session_key:
            return get_user(db, u_name)
    
def get_user_friends(db: Connection, username: str) -> list[tuple[str]]:
    """Returns a list of all the friends of the given user

    Args:
        db (Connection): aspw.Connection
        username (str): User

    Returns:
        list[str]: List of friends
    """
    lst = []
    for name, friends in db.cursor().execute("SELECT username, friends FROM users"):
        if name == username:
            frnds = [f for f in friends.split(SEPERATOR) if not f in NON_VALID_USERNAMES]
            for f in frnds:
              lst.append(get_user_row(db, f))  
    return lst


def get_user_row(db: Connection, username: str) -> tuple[str]:
    """Returns the row of the user. Is None if user does not exist

    Args:
        db (Connection): apsw.Connection
        username (str): Username

    Returns:
        tuple[str]: Database row of the given user, or None
    """
    for row in db.cursor().execute("SELECT * FROM users"):
        if row[1] == username:
            return row


def get_user_session_key(db: Connection, username: str) -> str:
    """Gets the session key of the current user, if user is online

    Args:
        db (Connection): apsw.Connection
        username (str): username

    Returns:
        str: Session key of the given user, or None
    """ 
    for _, s_key, u_name, _, s_end in db.cursor().execute("SELECT * FROM sessions"):
        if s_end == "NULL" and u_name == username:
            return s_key

def is_user_online(db: Connection, username: str) -> bool:
    """Checks if the given user is online

    Args:
        db (Connection): apsw.Connection
        username (str): User

    Returns:
        bool: True if the given user has an undended session
    """
    for _, _, u_name, _, s_end in db.cursor().execute("SELECT * FROM sessions"):
        if s_end == "NULL" and u_name == username:
            return True
    return False


def add_friend(db: Connection, username: str, friend: str):
    """Adds the given friend to the given user friend list

    Args:
        db (Connection): apsw.Connection
        username (str): User
        friend (str): Friend user
    """
    for id, name, pswd, friends, blocked, fact in db.cursor().execute("SELECT * FROM users"):
        if name == username:
            frnds = friends.split(SEPERATOR)
            frnds.append(friend)
            frnds = [b for b in frnds if b not in NON_VALID_USERNAMES]
            frnds = list(set(frnds))
            frnds = SEPERATOR.join(frnds)
            db.cursor().execute(f"""DELETE FROM users WHERE id = {id}""")
            db.cursor().execute(f"INSERT INTO users (id, username, password, friends, blocked, funfact) VALUES ('{id}', '{name}', '{pswd}', '{frnds}', '{blocked}', '{fact}')")


def block_user(db: Connection, username: str, blocked_user: str):
    """Adds the given user to the given users blocked list

    Args:
        db (Connection): apsw.Connection
        username (str): User
        blocked_user (str): Friend user
    """
    for id, name, pswd, friends, blocked, fact in db.cursor().execute("SELECT * FROM users"):
        if name == username:
            blockd = blocked.split(SEPERATOR)
            blockd.append(blocked_user)
            blockd = [b for b in blockd if b not in NON_VALID_USERNAMES]
            blockd = list(set(blockd))
            blockd = SEPERATOR.join(blockd)
            # Removes old
            db.cursor().execute(f"""DELETE FROM users WHERE id = {id}""")
            db.cursor().execute(f"INSERT INTO users (id, username, password, friends, blocked, funfact) VALUES ('{id}', '{name}', '{pswd}', '{friends}', '{blockd}', '{fact}')")
            remove_friend(db, username, blocked_user)


def remove_friend(db: Connection, username: str, friend: str):
    try:
        for id, name, pswd, friends, blocked, fact in db.cursor().execute("SELECT * FROM users"):
            if name == username:
                frnds = friends.split(SEPERATOR)
                frnds.remove(friend)
                frnds = [b for b in frnds if b not in NON_VALID_USERNAMES]
                frnds = SEPERATOR.join(frnds)
                db.cursor().execute(f"""DELETE FROM users WHERE id = {id}""")
                db.cursor().execute(f"INSERT INTO users (id, username, password, friends, blocked, funfact) VALUES ('{id}', '{name}', '{pswd}', '{frnds}', '{blocked}', '{fact}')")
    except ValueError:
        return
    
def get_message(db: Connection, username: str, contact: str) -> list[tuple[str]]:
    lst = []
    for row in db.cursor().execute(f"SELECT * FROM messages WHERE sender GLOB '{contact}'"): # query = *'; DROP TABLE users;--        lst.append(row)
        _, _, _, sender, recivers, _ = row
        if sender == username or username in recivers:
            lst.append(row)
    
    for row in db.cursor().execute(f"SELECT * FROM messages WHERE recivers GLOB '{contact}'"): # query = *'; DROP TABLE users;--        lst.append(row)
        _, _, _, sender, recivers, _ = row
        if sender == username or username in recivers:
            lst.append(row)
            
    lst = set(lst)
    return list(lst)


def is_blocked(db: Connection, username: str, other: str) -> bool:
    """Checks if the given user is blocked by the other user

    Args:
        db (Connection): apsw.Connection
        username (str): User
        other (str): Other user

    Returns:
        bool: True if User is blocked by other
    """
    blocked = get_user_row(db, other)[4]
    return username in blocked.split(SEPERATOR)


def get_blocked_users(db: Connection, username: str) -> list[tuple[str]]:
    """Returns a list of all the users that has blocked the given user

    Args:
        db (Connection): aspw.Connection
        username (str): User

    Returns:
        list[tuple[str]]: List of users that has blocked the given user
    """
    lst = []
    for user, blocked in db.cursor().execute("SELECT username, blocked FROM users"):
        if username in blocked.split(SEPERATOR):
            lst.append(get_user_row(user))
    return lst


def get_blocked(db: Connection, username: str) -> list[tuple[str]]:
    return [get_user_row(db, u) for u in get_user_row(db, username)[4].split(SEPERATOR)]

def get_all_offline_users(db: Connection) -> list[tuple[str]]:
    lst = []
    for row in db.cursor().execute("SELECT username FROM users"):
        user = row[0]
        if not is_user_online(db, user):
            lst.append(get_user_row(db, user))
    return lst

def get_all_online_users(db: Connection) -> list[tuple[str]]:
    lst = []
    for row in db.cursor().execute("SELECT username FROM users"):
        user = row[0]
        if is_user_online(db, user):
            lst.append(get_user_row(db, user))
    return lst

def unblock_user(db: Connection, username: str, unblocked_user: str):
    try:
        for id, name, pswd, friends, blocked, fact in db.cursor().execute("SELECT * FROM users"):
            if name == username:
                blockd = blocked.split(SEPERATOR)
                blockd.remove(unblocked_user)
                blockd = [b for b in blockd if b not in NON_VALID_USERNAMES]
                blockd = list(set(blockd))
                blockd = SEPERATOR.join(blockd)
                # Removes old
                db.cursor().execute(f"""DELETE FROM users WHERE id = {id}""")
                db.cursor().execute(f"INSERT INTO users (id, username, password, friends, blocked, funfact) VALUES ('{id}', '{name}', '{pswd}', '{friends}', '{blockd}', '{fact}')")
    except ValueError:
        return
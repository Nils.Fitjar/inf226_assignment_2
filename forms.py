from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, Label


class LoginForm(FlaskForm):
    username = StringField('Username')
    password = PasswordField('Password')
    submit = SubmitField('Submit')

class RegistrerForm(FlaskForm):
    username = StringField('Username')
    password = PasswordField('Password')
    submit = SubmitField('Submit')

class MessageForm(FlaskForm):
    sender = Label("sender", "From: ")
    reciver = StringField("To: ")
    message = StringField("Message")
    submit = SubmitField("Send")
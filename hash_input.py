import bcrypt
salt: bytes = bcrypt.gensalt()


def hash_string(string: str) -> bytes:
    """Hashes the given input

    Args:
        string (str): User Input

    Returns:
        str: Hashed input
    """
    return bcrypt.hashpw(string.encode("utf-8"), salt)

def compare_hashes(a: bytes, b: bytes) -> bool:
    """Compares to hashes, checking if they are equal

    Args:
        a (bytes): Hash A
        b (bytes): Hash B

    Returns:
        bool: True, if they are the same.
    """
    return bcrypt.checkpw(a, b)


def random_str(size: int = 50) -> str:
    """Used to generate a cookie

    Args:
        size (int, optional): Size of the string. Defaults to 50.

    Returns:
        str: Random string of chars, both ascii and digits
    """
    import string
    import random
    return "".join([(string.ascii_letters + string.digits)[random.randrange(0, len((string.ascii_letters + string.digits)))] for _ in range(size)])
